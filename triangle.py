from math import *
from random import randint

import pyglet
from pyglet.gl import *

import colors

from colors import linecolor as linecolor

color= "red"


class graphicsWindow(pyglet.window.Window):
    def __init__(self):
        super(graphicsWindow, self).__init__()              # constructor for graphicsWindow class
        self.center1 = [self.width / 2, self.height / 2]    # initialize the centre of the triangle
        self.center2 = [self.width / 2, self.height / 2]    # initialize the centre of the triangle


    def calc_vertices(self, numberOfVertices):


        radius = 20                 # specify the radius of each point from the center
        xcenter1 = self.center1[0]   # specify xcenter
        ycenter1 = self.center1[1]   # specify ycenter
        vertices1 = []  # initialize a list of vertices

        xcenter2 = self.center2[0]  # specify xcenter
        ycenter2 = self.center2[1]  # specify ycenter
        vertices2 = []  # initialize a list of vertices

        for i in range(0, numberOfVertices):
            angle = (float(i * 2) / numberOfVertices) * pi  # specify the (i+1)th vertex of the triangle (x,y values)
            x = radius * cos(angle) + xcenter1
            y = radius * sin(angle) + ycenter1
            vertices1.append(x)  # append the x value to the vertex list
            vertices1.append(y)  # append the y value to the vertex list

            angle = (float(i * 2) / numberOfVertices) * pi  # specify the (i+1)th vertex of the triangle (x,y values)
            x = radius * cos(angle) + xcenter2
            y = radius * sin(angle) + ycenter2
            vertices2.append(x)  # append the x value to the vertex list
            vertices2.append(y)  # append the y value to the vertex list

        return (vertices1, vertices2)

    def update(self, dt):
        print "Updating the center of the triangle"
        self.center1 = [self.width / 2 + randint(-200, 200), self.height / 2 + randint(-200, 200)]
        self.center2 = [self.width / 2 + randint(-200, 200), self.height / 2 + randint(-200, 200)]

    def on_draw(self):

        # now we will calculate the list of vertices required to draw the triangle

        numberOfVertices = 100        # specify the number of vertices we need for the shape

        (vertices1, vertices2) = self.calc_vertices(numberOfVertices)


        # convert the vertices list to pyGlet vertices format
        vertexList1 = pyglet.graphics.vertex_list(numberOfVertices, ('v2f', vertices1))
        vertexList2 = pyglet.graphics.vertex_list(numberOfVertices, ('v2f', vertices2))

        # now use pyGlet commands to draw lines between the vertices
        glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)  # clear the graphics buffer
        glColor3f(linecolor[color][0], linecolor[color][1], linecolor[color][2])                      # specify colors
        vertexList1.draw(GL_LINE_LOOP)           # draw
        vertexList2.draw(GL_LINE_LOOP)           # draw


# this is the main game engine loop
if __name__ == '__main__':
    window = graphicsWindow()   # initialize a window class
    pyglet.clock.schedule_interval(window.update, 1 / 2.0)  # tell pyglet the on_draw() & update() timestep
    pyglet.app.run() # run the infinite pyglet loop